//
//  KategoriViewController.swift
//  tes
//
//  Created by Jeslyn Kosasih on 17/06/20.
//  Copyright © 2020 afitra mamor. All rights reserved.
//

import UIKit

class KategoriViewController: UIViewController {

    @IBOutlet var kategoriCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        kategoriCollectionView.delegate = self
        kategoriCollectionView.dataSource = self
        // Do any additional setup after loading the view.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension KategoriViewController: UICollectionViewDelegate, UICollectionViewDataSource{

func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return 1
}

func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "KategoriCollectionViewCell", for: indexPath) as! KategoriCollectionViewCell
    
    cell.cellImageView.image = UIImage(named: "kategori")
    cell.cellJudulLabel.text = "Kamping"
    
    return cell
}

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "kampingToDetail", sender: indexPath.item)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "kampingToDetail" {
            var destinationVC = segue.destination as! detailCeritaViewController
        
        }
    }
}
