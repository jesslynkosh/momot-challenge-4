//
//  KategoriCollectionViewCell.swift
//  tes
//
//  Created by Jeslyn Kosasih on 17/06/20.
//  Copyright © 2020 afitra mamor. All rights reserved.
//

import UIKit

class KategoriCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var cellImageView: UIImageView!
    @IBOutlet var cellJudulLabel: UILabel!
    
}
