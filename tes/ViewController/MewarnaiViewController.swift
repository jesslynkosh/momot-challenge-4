//
//  MewarnaiViewController.swift
//  tes
//
//  Created by afitra mamor on 29/05/20.
//  Copyright © 2020 afitra mamor. All rights reserved.
//

import UIKit

class MewarnaiViewController: UIViewController {
    
    
    
    
//    var allData : [Any] = []
    var allData:[(
        isRead:Bool,
        finish:Bool,
        navigationNext:Bool,
        navigationBack:Bool,
        narasi:Bool,
        dialogAyah:Bool,
        objekModal:Bool,
        choise:Bool,
        isBackground:String,
        isNarasi:String,
        isDialogAyah:String,
        isChoise:(
                    setIndex:[Int],
                    changeBackground:[String],
                    toChange:Bool,
                    pattern:[
                    (
                    type:String,
                    iconImage:String,
                    setBackground:String,
                    wordTitle:String
                    )
                    ]
        ),
        isModal:
        (
        wordTitle:String,
        photo:String,
        isGif:String,
        iconImage:String,
        containerImage:[String]))] = []
    
    
    @IBOutlet weak var photoLabel: UIImageView!
    
    
    @IBOutlet weak var Hijau: UIButton!
    
    
    @IBOutlet weak var Oranye: UIButton!
    @IBOutlet weak var Pink: UIButton!
    
    @IBOutlet weak var Kuning: UIButton!
    @IBOutlet weak var Biru: UIButton!
    @IBOutlet weak var Dongker: UIButton!
    
    
    @IBOutlet weak var Ungu: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        _ = startColor()
        
        _ = setImage()
        _ = getData()
        // Do any additional setup after loading the view.
    }
    
    func getData( ) -> String {
        
        if let path = Bundle.main.path(forResource: "barang", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                do{
                    
                    let jsonResponse = try JSONSerialization.jsonObject(with:
                        data, options: [])
                    //                        print(jsonResponse)
                    guard let jsonArray = jsonResponse as? [[String: Any]] else {
                        return "false"
                    }
                    
                    
                    for (i,item) in jsonArray.enumerated(){
                        var temp : (
                        isRead:Bool,
                        finish:Bool,
                        navigationNext:Bool,
                        navigationBack:Bool,
                        narasi:Bool,
                        dialogAyah:Bool,
                        objekModal:Bool,
                        choise:Bool,
                        isBackground:String,
                        isNarasi:String,
                        isDialogAyah:String,
                        isModal:
                                (
                                   wordTitle:String,
                                   photo:String,
                                   isGif:String,
                                   iconImage:String,
                                   containerImage:[String]
                                  ),
                        isChoise:(
                                 setIndex:[Int],
                                 changeBackground:[String],
                                 toChange:Bool,
                        pattern:[String:String]
                            )
                       
                        )
                        
                        
                        // ================================================= BATAS LULUS TESTING DATA TANPA OPSIONAL
                        temp.isRead = item["isRead"] as? Bool ?? false
                        temp.finish   =   item["finish"] as? Bool ?? false
                        temp.navigationNext = item["navigationNext"] as? Bool ?? false
                        temp.navigationBack = item["navigationBack"] as? Bool ?? false
                        temp.narasi = item["narasi"] as? Bool ?? false
                        temp.dialogAyah = item["dialogAyah"]  as? Bool ?? false
                        temp.objekModal = item["objekModal"] as? Bool ?? false
                        temp.choise = item["choise"] as? Bool ?? false
                        temp.isBackground = item["isBackground"] as? String ?? ""
                        temp.isNarasi = item["isNarasi"] as? String ?? ""
                        temp.isDialogAyah = item["isDialogAyah"] as? String ?? ""
                        
                        // ================================================= BATAS LULUS TESTING DATA TANPA OPSIONAL
                        
                        let modal = item["isModal"] as? [String : Any]
 
                       
                        temp.isModal.wordTitle = modal?["wordTitle"] as? String ?? ""
                        temp.isModal.photo = modal?["photo"] as? String ?? ""
                        temp.isModal.isGif = modal?["isGif"] as? String ?? ""
                        temp.isModal.iconImage = modal?["iconImage"] as? String ?? ""
                        temp.isModal.containerImage = modal?["containerImage"] as? [String] ?? []
                      
                        // ================================================= BATAS LULUS TESTING DATA TANPA OPSIONAL
                        
                        
//                        let choise = jsonArray[0]["isChoise"] as? [String : Any]
                        let choise = item["isChoise"] as? [String : Any]

                        temp.isChoise.setIndex = choise?["setIndex"]  as? [Int] ?? []
 
                        temp.isChoise.changeBackground = choise?["changeBackground"] as? [String] ?? []
                        temp.isChoise.toChange =  choise?["toChange"] as? Bool ?? false
                        
                        let pattern = choise?["pattern"] as? Array<Any>  ?? [(
                                               type:"",
                                               iconImage:"",
                                               setBackground:"",
                                               wordTitle:""
                                               )]
                        
                       
 
                        allData.append(
                        (
                            isRead:temp.isRead,
                            finish:temp.finish,
                            navigationNext:temp.navigationNext,
                            navigationBack:temp.navigationBack,
                            narasi:temp.narasi,
                            dialogAyah:temp.dialogAyah,
                            objekModal:temp.objekModal,
                            choise:temp.choise,
                            isBackground:temp.isBackground,
                            isNarasi:temp.isNarasi,
                            isDialogAyah:temp.isDialogAyah,
                            isChoise:(setIndex: [],
                            changeBackground: [],
                            toChange: temp.isChoise.toChange,
                            pattern: [
                                ( type : " ", iconImage: " ", setBackground: " ", wordTitle: " " ),
                                ( type : " ", iconImage: " ", setBackground: " ", wordTitle: " " )
                            ]),
                            isModal:temp.isModal))
                        
               
                        
                    }
                    
                    
 
                    
                    
                    
                } catch let parsingError {
                    print("Error", parsingError)
                }
            } catch {
                // Handle error here
                print("erro ")
            }
        }
        //        print()
        return "true"
        
    }
    
    func setImage() -> Bool {
        
        
        //        photoLabel.contentMode = UIView.ContentMode.scaleAspectFit
        
        //        photoLabel.center = self.view.center
        photoLabel.image = UIImage(named: "tes-image")
        photoLabel.backgroundColor = getUIColor(hex: "EDE9F4")
        photoLabel.layer.cornerRadius = 20
        //        photoLabel.clipsToBounds = true
        return true
    }
    
    
    @IBAction func changeOrange(_ sender: Any) {
        //
    }
    
    @IBAction func changePink(_ sender: Any) {
        //
    }
    
    @IBAction func changeKuning(_ sender: Any) {
    }
    
    @IBAction func changeHijau(_ sender: Any) {
    }
    
    @IBAction func changeBiru(_ sender: Any) {
    }
    
    
    @IBAction func changeDongker(_ sender: Any) {
    }
    
    
    @IBAction func changeUngu(_ sender: Any) {
    }
    func getUIColor(hex: String, alpha: Double = 1.0) -> UIColor? {
        var cleanString = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cleanString.hasPrefix("#")) {
            cleanString.remove(at: cleanString.startIndex)
        }
        
        if ((cleanString.count) != 6) {
            return nil
        }
        
        var rgbValue: UInt32 = 0
        Scanner(string: cleanString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func startColor() -> Bool {
        
        let allButtonCollor = [
            (
                button:Oranye,
                color:"E81C24"
            ),
            (
                button:Pink,
                color:"E20082"
            ),
            (
                button:Kuning,
                color:"FFE800"
            ),
            (
                button:Hijau,
                color:"009C51"
            ),
            (
                button:Biru,
                color:"00A4E5"
            ),
            (
                button:Dongker,
                color:"2E3188"
            ),
            (
                button:Ungu,
                color:"9261EE"
            )
        ]
        
        for item in allButtonCollor{
            item.button?.backgroundColor = getUIColor(hex:item.color)
            item.button?.setTitle("" , for: .normal)
            item.button?.layer.cornerRadius = 15
            item.button?.layer.borderWidth = 0.3
            item.button?.layer.borderColor = UIColor.black.cgColor
        }
        
        return true
    }
    
}
